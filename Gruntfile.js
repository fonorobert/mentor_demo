module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		
		jshint: {
			options: {
				sub: true
			},
			gruntfile: 'Gruntfile.js',
			dev: 'src/js/*.js'
		},

		lesslint: {
			src: 'src/less/*.less',
			options: {
				csslint: {
					ids: false
				}
			}
		},

		jsonlint: {
		  all: {
		    src: [ 'res/*.json' ]
		  }
		},

		concat: {
			dev: {
				src: ['node_modules/jquery/dist/jquery.min.js', 'src/js/*.js'], 
				dest: 'js/main.js'
			},
			options: {
				'banner': '/*! <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %> */',
				'sourceMap': true
			},
		},

		less: {
			dev: {
				options: {
					'sourceMap': true,
					'banner': '/*! <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %> */'
				},
				files: {
					'css/main.css': ['src/less/import.less']
				}
			},
			prod: {
				options: {
					'banner': '/*! <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %> */',
					'cleancss': true
				},
				files: {
					'css/main.min.css': ['src/less/import.less', 'src/less/var.less', 'src/less/structure.less', 'src/less/type.less', 'src/less/sections.less']
				}
			}
		},

		uglify: {
			options: {
				'banner': '/*! <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %> */',
				'mangle': false
			},
			prod: {
				files: {
					'js/main.min.js': ['node_modules/jquery/dist/jquery.min.js', 'src/js/*.js']
				}
			}
		},
		convert: {
	    options: {
	      explicitArray: false
	    },
	    csv2json: {
	      src: ['res/feedback.csv'],
	      dest: 'res/feedback.json'
	    }
  	},
		
		watch: {
			js: {
				files: ['src/js/*.js'], 
				tasks: ['jshint:dev', 'concat:dev']
			},
			gruntfile: {
				files: ['Gruntfile.js'],
				tasks: ['jshint:gruntfile']
			},
			less: {
				files: ['src/less/*.less'],
				tasks: ['less:dev']
			},
			json: {
				files: ['res/*.json'],
				tasks: ['jsonlint:all']
			},
			csv2json: {
				files: ['res/*.csv'],
				tasks: ['convert:csv2json']
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-convert');
	grunt.loadNpmTasks('grunt-jsonlint');

	grunt.registerTask('default', ['jshint:dev', 'concat:dev', 'less:dev', 'convert:csv2json', 'jsonlint:all']);
	grunt.registerTask('prod', ['jshint:dev', 'uglify:prod', 'less:prod']);
};