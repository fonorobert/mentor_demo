function menuToggle(){
	$('nav #caret').toggleClass('flip');
	$('nav ul').slideToggle('fast');

}
$('#menuToggle').click(function(){
	menuToggle();
});

function tableColor(el) {
	$(el).find('td').each(function(){
		var cont = $(this).text();
		switch(cont) {
			case 'Igen':
				$(this).addClass('true');
				break;
			case 'Nem':
				$(this).addClass('false');
				break;
			case 'Speciális':
				$(this).addClass('warning');
				break;
		}
	});
}
$(document).ready(function(){
	tableColor('#profileSummary');	
});