//Instantiate Profile and Summary classes
if($('body').hasClass('profile')){
	var profile = new Profile();
	profile.build();
	var summary = new Summary();
	summary.build();
	var page = new Page(profile, summary);
	page.build();
}

if ($('body').hasClass('knowledge')){
	var knowledge = new Knowledge();
	knowledge.build();
}

var name = $('td[data-key="Mentorált"]').text();

function Page(OBJprofile, OBJsummary) {
	var that = this;
	this.profile = OBJprofile;
	this.summary = OBJsummary;

	this.configURL = "res/config.json";
	this.name = $('td[data-key="Mentorált"]').text();

	this.config = window.config;
	this.data = window.feedback;

	this.build = function() {
		if (this.data === undefined || this.data === false)
		{
			this.load();
			return false;
		}

		$('#summary tbody tr').click(function(){
			var td = $(this).children('td[data-key="Mentorált"]');
			that.showProfile(td);
		});
		$('#back').click(function(){
			that.showProfile();
		});
	};

	this.showProfile = function(element)
	{
			var name = $(element).text();
			$(this.config.fields.name).html(name);
			this.profile.build();
			$(this.config.fields.summary).fadeToggle();
			$(this.config.fields.profile).fadeToggle();
			this.scroll('header');
	};
 
	this.scroll = function(el)
	{
		$("html, body").animate({scrollTop: $(el).offset().top}, 500);
	};
	
}