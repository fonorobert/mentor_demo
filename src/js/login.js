var login = 'mentor';
var pass = 'mentor';

$('form.login').submit(function(e){
	e.preventDefault();
	console.log($(this));
	if($('input#username').val().toLowerCase() === login && $('input#pass').val().toLowerCase() === pass) {
		window.location = 'profile.html';
	} else {
		$('#loginError').fadeIn();
		$('input#username').focus();
	}
});

function filler() {
	if(!$('body').hasClass('login')) {
		return;
	}
	var windowHeight = $(window).height();
	var bodyHeight = $('body').height();
	if(bodyHeight < windowHeight) {
		$('#filler').height(windowHeight - bodyHeight);
	}
}

$(document).ready(function(){
	filler();
});

$(window).resize(function(){
	filler();
});