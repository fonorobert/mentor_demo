function Profile() {
	var self = this;
	this.configURL = "res/config.json";
	this.config = window.config;

	this.name = "";
	this.profile = "";
	//var profileNo;
	this.data = window.feedback;
	//this.info = {"0": false};

	this.build = function(fields){

		this.name = this.getName(this.config.fields.name);
		this.profile = this.getProfile({"data": this.data, "name": this.name});
		
		this.loadInfo({"profile": this.profile, "name": this.name});
		this.loadFeedback({"profile": this.profile});
	};

	this.getName = function(field){
		var name = $(field).html();
		return name;
	};

	this.getProfile = function(args){
		var data = args.data;
		var name = args.name;
		var profile = "";
		for(var key in data)
		{
			if (data[key].Mentorált === name)
			{
				profile = data[key];
			}
		}
		return profile;
	};

	this.loadInfo = function(args){
		var fields = self.config.fields;
		var profile = args.profile;

		var phone;
		var email;
		var mentor;

		$(fields.mentor).html(profile["Mentor"]);
		$(fields.phone).html(profile["Telefon"]);
		$(fields.email).html(profile["E-mail"]);
		return true;
	};

	this.loadFeedback = function(args){
		var field = self.config.fields.feedback;
		var profile = args.profile;
		var exp = new RegExp(this.config.infoFields);

		$(field).empty();
		for(var key in profile)
		{
			if (!key.match(exp) && profile[key] !== "")
			{
				var feedback = '<h5>' + key + ':</h5>' + '<p>' + profile[key] + '</p>';
				$(field).append(feedback);
			}
		}
	};
}