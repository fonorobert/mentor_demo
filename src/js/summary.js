function Summary() {
	var self = this;
	this.configURL = "res/config.json";
	this.config = window.config;
	this.data = window.feedback;


	this.build = function(fields){
		
		//Declare private vars of method
		var table = "";
		var exp = new RegExp(this.config.summaryFields);

		//Build table header
		table += "<thead><tr>";
		for (var key in this.data[0])
		{
			if (key.match(exp))
			{
				table += "<th>" + key + "</th>";
			}
		}
		table += "</tr></thead>";

		//Build table content
		for (var rec in this.data)
		{
			var record = this.data[rec];
			table += '<tr>';
			for (var i in record)
			{
				if(i.match(exp))
				{
					table += '<td data-key="' + i + '">' + record[i] + '</td>';
				}
			}
			table += '</tr>';
		}


		$(this.config.fields.summaryTable).html(table);
	};
}